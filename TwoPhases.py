from FileManager import FileManager
from Simplex import Simplex

import math


class TwoPhases(Simplex):
    u_row_aux = []

    def __init__(self, problem, filename):

        super().__init__(problem, filename)

    def set_up(self):
        for i, operation in enumerate(self.operators):

            if operation == '<=':  # +S
                self.add_var(i, 1)
                self.u_row.append(0)

            elif operation == '>=':  # -E +R
                self.add_var(i, -1)
                self.add_var(i, 1)
                self.u_row.append(0)
                self.u_row.append(0)

            else:  # +R
                self.add_var(i, 1)
                self.u_row.append(0)

    def set_up_aux(self):

        r_index = []

        self.u_row_aux = self.u_row
        self.u_row = [x - x for x in self.u_row]

        for i, operation in enumerate(self.operators):

            if operation == '<=':  # +S
                self.add_var(i, 1)
                self.u_row.append(0)

            elif operation == '>=':  # -S +R
                self.add_var(i, -1)
                self.add_var(i, 1)
                self.u_row.append(0)
                self.u_row.append(-1)
                r_index.append(len(self.u_row) - 1)

            else:  # +R
                self.add_var(i, 1)
                self.u_row.append(-1)
                r_index.append(len(self.u_row) - 1)

        return r_index

    def search_row(self, index):

        for i, row in enumerate(self.constrains):

            if row[index] == 1:
                return i

    def two_phases_solve(self):

        # I fase, se calcula la nueva función objetivo

        indexes = self.set_up_aux()

        for i, column in enumerate(indexes):
            f = self.search_row(column)

            for j, value in enumerate(self.constrains[f]):
                self.u_row[j] += value

            self.results[0] += self.results[f + 1]

        self.u_row = [-x for x in self.u_row]
        self.results[0] *= -1

        self.solve()

        self.u_row = self.u_row_aux
        self.setup_u_row()

        deletes = 0

        for column in indexes:

            for row in self.constrains:
                row.pop(column - deletes)

            deletes += 1


"""b = TwoPhases(FileManager.process_file("problem2.txt"))
b.two_phases_solve()
b.solve()"""

"""
degenerada: si cuando se obtiene el pivote y dos cocientes dan iguales
no acotada: cuando se busca el pivote y todos los candidatos para la filla pivote son menores o iguales a cero
solución multiple: en la función objetivo el valor inmediato debajo de ella es 0 y que esa x no se encuentre en las variables básicas
se tiene que volver a iterar usando de columna pivote esa x.
no factible: cuando se resuelve y las soluciones no cumpen las restricciones
no acotada dos fases: si el último campo de la U no es cero no se pasa a fase 2
"""
