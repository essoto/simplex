from Problem import Problem


class FileManager:

  @staticmethod
  def is_number(n):
    try:
      float(n)
    except ValueError:
      return False
    return True

  @staticmethod
  def process_line(line, restricccions_array, matrix, line_count):

    new_row = []
    i = 0

    while i < len(line):
      if i == (len(line) - 2) and line_count > 1:
        if line[i] == "<=" or line[i] == ">=" or line[i] == "=":
          restricccions_array.append(line[i])
        else:
          return "Símbolo no identificado"
      else:
        if FileManager.is_number(line[i]):
          new_row.append(float(line[i]))
      i += 1

    matrix.append(new_row)

  @staticmethod
  def generateSolutionFileName(file_name):
    FileName = (file_name.split('.')[:-1])[0]
    extensionFileName = (file_name.split('.')[-1:])[0]
    solutionFileName = FileName + '_sol.' + extensionFileName
    return solutionFileName

  @staticmethod
  def fileWriter(file_name, text):
    try:
      file = open(file_name, 'a')
      file.write(text)
      file.close()
    except:
      print("El archivo no existe o el nombre es incorrecto.")

  @staticmethod
  def process_file(file_name):

    try:

      file = open(file_name, 'r')

      line_count = 0

      method = -1
      objective = ""
      restriccions_array = []
      matrix = []

      for line in file.readlines():
        line = line.strip("\n")
        line_splitted_by_comma = line.split(",")

        if line_count == 0:
          if line_splitted_by_comma[0].isnumeric():
            method = int(line_splitted_by_comma[0])

          if line_splitted_by_comma[1] == "max" or line_splitted_by_comma[1] == "min":
            objective = line_splitted_by_comma[1]

          else:
            raise Exception("Error al procesar el  archivo")

        else:
          FileManager.process_line(line_splitted_by_comma, restriccions_array, matrix, line_count)

        line_count += 1

      matrix[0].append(0)
      return Problem(method, objective, restriccions_array, matrix)

    except:
      print("El archivo no existe o hay un error dentro del mismo.")


"""a = FileManager.process_file("big_m_example.txt")
#b = FileManager.generateSolutionFileName("TestFile.txt")
a.print_properties()"""
