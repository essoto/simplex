class Problem:

    method = -1
    objective = ""
    restrictions = []
    matrix = []

    def __init__(self, method, objective, restrictions, matrix):
        self.method = method
        self.objective = objective
        self.restrictions = restrictions
        self.matrix = matrix

    def print_properties(self):
        print(self.method)
        print(self.objective)
        print(self.restrictions)
        print(self.matrix)

    def get_u(self):
        return self.matrix[0].pop()

    def get_results(self):

        i = 0
        results = []

        while i < len(self.matrix):
            results.append(self.matrix[i][-1])
            i += 1

        return results

    def get_matrix_without_u(self):

        matrix_aux = self.matrix[1:]
        for i, j in enumerate(matrix_aux):
            matrix_aux[i].pop()
        return matrix_aux

    def get_objective(self):
        return self.objective

    def __get_method(self):
        return self.method

    def __get_matrix(self):
        return self.matrix

    def get_restrictions(self):
        return self.restrictions

    def __str__(self):
        return "Method: " + str(self.method) + "\n" \
               + "Objective: " + str(self.objective) + "\n" \
               + "Matrix: " + str(self.matrix)
