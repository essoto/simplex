import sys
from FileManager import FileManager
from Dual import Dual
from BigM import BigM
from TwoPhases import TwoPhases
from Simplex import Simplex


def main(argv):
    if len(sys.argv) == 1:
        print("No se recibieron argumentos")
        exit(0)
    elif sys.argv[1] == "-h":
        print("Formato de argumentos inválido")
        print("Formato válido: python3 main.py file.txt")

    else:

        num = -1
        for arg in sys.argv:

            num = num + 1

            if num == 0:
                print("Los archivos a procesar son:" + str(sys.argv[1:]))

            elif num > 0 and arg.endswith('.txt'):

                try:
                    print("Archivo" + str(num) + ":" + str(arg))
                    problem = FileManager.process_file(arg)

                    # 0=Simplex, 1=GranM, 2=DosFases, 3=Dual
                    if problem.method == 0:
                        try:
                            simplex = Simplex(problem, arg)
                            simplex.setup()
                            simplex.solve()
                        except:
                            return "Error al ejecutar simplex"

                    elif problem.method == 1:
                        try:
                            big_m = BigM(problem, arg)
                            big_m.setup()
                            big_m.solve()
                        except:
                            return "Error al ejecutar Gran M"

                    elif problem.method == 2:
                        try:
                            two_phases = TwoPhases(problem, arg)
                            two_phases.two_phases_solve()
                            two_phases.solve()

                        except:
                            print("error")
                            return "Error al ejecutar Dos fases"

                    elif problem.method == 3:

                        try:
                            #dual = Dual(problem)
                            #dual.solutionFileName = FileManager.generateSolutionFileName(arg)
                            #dual.printMatrix(c.problem.matrix, c.problem.restrictions)
                            #dual.problem.matrix = c.transpose()
                            #dual.problem.restrictions = c.convertRestriccions()
                            #dual.problem.objective = c.convertObjective()
                            #dual.printMatrix(c.problem.matrix, c.problem.restrictions)
                            #if (dual.problem.objective=='min'):
                            #  dual.convertToMax()
                            #dual.printMatrix(dual.problem.matrix, dual.problem.restrictions)
                            #dual.addBasicVar()
                            #dual.printMatrix(dual.problem.matrix, dual.problem.restrictions)
                            #dual.solve()
                            #dual.printMatrix(dual.problem.matrix, dual.problem.restrictions)
                            
                            dual = Dual(problem,arg)
                            dual.printMatrix(dual.problem.matrix, dual.problem.restrictions)
                            dual.problem.matrix = dual.transpose()
                            dual.problem.restrictions = dual.convertRestriccions()
                            dual.problem.objective = dual.convertObjective()

                            #simplex = Simplex(dual.problem)
                            #simplex.setup()
                            #simplex.solve()
                            #dual.printMatrix(dual.problem.matrix, dual.problem.restrictions)
                            
                            big_m = BigM(dual.problem, arg)
                            big_m.setup()
                            big_m.solve()
                        except:
                            print("error")
                            return "Error al ejecutar Dual"
                except:
                    print("El archivo presenta un problema")

if __name__ == '__main__':
    main(sys.argv[1:])
