import math
import sys

from FileManager import FileManager
from BigM import BigM

from copy import deepcopy
class Dual:
  problem = None
  matrix = []
  restriccions = []
  noNegatividad = []
  objective = ''
  filename = ''

  u_row = []
  results = []
  operators = []
  constrains = []
  
  def __init__(self, algorithm, filename):
    self.problem = algorithm
    self.filename = filename
    self.matrix = algorithm.matrix
    self.restriccions = algorithm.get_restrictions()
    i = 0
    while i < len(algorithm.matrix[0][:-1]):
      self.noNegatividad = self.noNegatividad + ['>=']
      i += 1

  def printMatrix(self, matrix, restriccions):
    solutionFileName = FileManager.generateSolutionFileName(self.filename)
    FileManager.fileWriter(solutionFileName,'------------------------------------------------------------------\n')
    print ('------------------------------------------------------------------')
    i = 0
    while i < len(matrix):
      j = 0
      row =''
      while j < len(matrix[i]):
        if (j+1)==len(matrix[i]):
          if i!=0:
            row = row + str(restriccions[i-1]) + '\t'
          else:
            row = row + '\t' 
        row = row + str(matrix[i][j]) + '\t'
        j += 1
      FileManager.fileWriter(solutionFileName,row+'\n')  
      print(row+'\n')
      i += 1
    FileManager.fileWriter(solutionFileName,'------------------------------------------------------------------\n\n')
    print ('------------------------------------------------------------------\n')
  
  def transpose(self):
    matrix = [list(x) for (x) in zip(*self.matrix)]
    result_matrix = deepcopy(matrix)
    result_matrix= [result_matrix[-1]]+result_matrix[0:-1]
    i = 0
    while i < len(result_matrix):
      result_matrix[i]= result_matrix[i][1:]+[result_matrix[i][0]]
      i += 1
    return result_matrix
    
  def convertRestriccions(self):
    result_matrix = deepcopy(self.noNegatividad)
    i = 0
    if(self.objective=='min'):
      while i < len(self.noNegatividad):
        if self.noNegatividad[i] == '<=':
          result_matrix[i]='>='
        if self.noNegatividad[i] == '=':
          result_matrix[i]='='
        if self.noNegatividad[i] == '>=':
          result_matrix[i]='<='
        i += 1
    if(self.objective=='max'):
      while i < len(self.noNegatividad):
        if self.noNegatividad[i] == '<=':
          result_matrix[i]='<='
        if self.noNegatividad[i] == '=':
          result_matrix[i]='='
        if self.noNegatividad[i] == '>=':
          result_matrix[i]='>='
        i += 1
    return result_matrix

  def convertNoNegatividad(self):
    result_matrix = deepcopy(self.restriccions)
    i = 0
    if(self.objective=='min'):
      while i < len(self.restriccions):
        if self.noNegatividad[i] == '<=':
          result_matrix[i]='<='
        if self.noNegatividad[i] == '=':
          result_matrix[i]='='
        if self.noNegatividad[i] == '>=':
          result_matrix[i]='>='
        i += 1
    if(self.objective=='max'):
      while i < len(self.restriccions):
        if self.noNegatividad[i] == '<=':
          result_matrix[i]='>='
        if self.noNegatividad[i] == '=':
          result_matrix[i]='='
        if self.noNegatividad[i] == '>=':
          result_matrix[i]='<='
        i += 1
    return result_matrix

  def convertObjective(self):
    if(self.problem.objective=='min'):
      objectiveDual='max'
    if(self.problem.objective=='max'):
      objectiveDual='min'
    return objectiveDual

  def get_pivot_column_1(self):
    min_index = 0
    min_item = math.inf

    for index, item in enumerate(self.u_row):
      if item < min_item and self.check_division_by_zero(index):
        min_item = item
        min_index = index
    print(min_index)
    return min_index

  def get_pivot_row_1(self, c):
    min_index = 0
    min_item = math.inf

    for index, row in enumerate(self.constrains):
      if row[c] > 0:
        if (self.results[index + 1] / row[c]) < min_item:
          min_item = self.results[index + 1] / row[c]
          min_index = index
    print(min_index)
    return min_index

  def get_pivot_column_2(self, r):
    min_index = 0
    min_item = math.inf
    max_dif = -math.inf

    for index, item in enumerate(self.constrains[r-1]):
      if((item<0 and self.u_row[index]>0) or (item>0 and self.u_row[index]<0)):
        if self.u_row[index]/item > max_dif:
          min_item = item
          min_index = index
          max_dif = self.u_row[index]/item
    return min_index

  def get_pivot_row_2(self):
    min_index = 0
    min_item = math.inf
    
    for index, item in enumerate(self.results):
      if item < min_item and item<0:
        min_item = item
        min_index = index
    return min_index

  def reduce(self):
    if (self.problem.objective == '-max'):
      r = self.get_pivot_row_2()
      c = self.get_pivot_column_2(r)
    else:
      c = self.get_pivot_column_1()
      r = self.get_pivot_row_1(c)

    pivot_value = self.constrains[r][c]

    for k, value in enumerate(self.constrains[r]):
      self.constrains[r][k] = value / pivot_value

    self.results[r + 1] = self.results[r + 1] / pivot_value

    for i, row in enumerate(self.constrains):
      if i != r:
        aux_value = self.constrains[i][c]

        for j, value in enumerate(self.constrains[i]):
          self.constrains[i][j] = value - (aux_value * self.constrains[r][j])
        self.results[i + 1] = self.results[i + 1] - (aux_value * self.results[r + 1])
    aux_value = self.u_row[c]

    for i, value in enumerate(self.u_row):
      self.u_row[i] = value - (aux_value * self.constrains[r][i])
    self.results[0] = self.results[0] - (aux_value * self.results[r + 1])

  def is_done(self):
    for value in self.u_row:
      if value < 0 and (self.problem.objective == 'max' or self.problem.objective == '-max'):
        return False
      if value > 0 and (self.problem.objective == 'min'):
        return False
    return True

  def solve(self):
    self.u_row = deepcopy(self.problem.matrix[0][:-1])
    self.results = self.problem.get_results()
    self.operators = self.problem.get_restrictions()
    self.constrains = self.problem.get_matrix_without_u()
    while not self.is_done():
      self.printMatrix(self.problem.matrix, self.problem.restrictions)
      self.reduce()
    self.printMatrix(self.problem.matrix, self.problem.restrictions)

  def check_division_by_zero(self, index):
    for row in self.constrains:
      if row[index] > 0:
        return True
    return False

  def convertToMax(self):
    i = 0
    while i < len(self.problem.matrix):
      j = 0
      while j < len(self.problem.matrix[i]):
        self.problem.matrix[i][j] = -self.problem.matrix[i][j]
        j += 1
      i += 1

    k = 0
    while k < len(self.problem.restrictions):
      if(self.problem.restrictions[k]== '<='):
        self.problem.restrictions[k]= '>='
      if(self.problem.restrictions[k]== '>='):
        self.problem.restrictions[k]= '<='
      k += 1
    self.problem.objective = '-max'

  def addBasicVar(self):
    i = 0

    while i < len(self.problem.matrix):
      if i==0:
        if (self.problem.objective=='max' or self.problem.objective=='-max'):
          j = 0
          while j < len(self.problem.matrix[i]):
            self.problem.matrix[i][j] = -self.problem.matrix[i][j]
            j += 1
        else:
          self.problem.objective='-max'
          
      if self.problem.restrictions[i - 1] == '<=' and i!=0:
        j = 0
        while j < len(self.problem.matrix):
          if (i == j):
            self.problem.matrix[j] = self.problem.matrix[j][0:-1] + [1] + [self.problem.matrix[j][-1]]
          else:
            self.problem.matrix[j] = self.problem.matrix[j][0:-1] + [0] + [self.problem.matrix[j][-1]]
          j += 1

      if self.problem.restrictions[i - 1] == '=' and i!=0:
        j = 0
        while j < len(self.problem.matrix):
          if (i == j):
            self.problem.matrix[j] = self.problem.matrix[j][0:-1] + [1] + [self.problem.matrix[j][-1]]
          else:
            self.problem.matrix[j] = self.problem.matrix[j][0:-1] + [0] + [self.problem.matrix[j][-1]]
          j += 1

      if self.problem.restrictions[i - 1] == '>=' and i!=0:
        j = 0
        while j < len(self.problem.matrix):
          if (i == j):
            self.problem.matrix[j] = self.problem.matrix[j][0:-1] + [-1, 1] + [self.problem.matrix[j][-1]]
          else:
            self.problem.matrix[j] = self.problem.matrix[j][0:-1] + [0, 0] + [self.problem.matrix[j][-1]]
          j += 1

      i += 1

  def print_table(self):
    x = FileManager.generateSolutionFileName(self.filename)
    with open(x, 'a') as f:
      for value in self.u_row:
        print('%7.1f,' % value, end='', file=f)
      print('%7.1f' % self.results[0], file=f)

      for i, row in enumerate(self.constrains):
        for j, value in enumerate(row):
          print('%7.1f,' % value, end='', file=f)
        print('%7.1f' % self.results[i + 1], file=f)
      print('-' * (8 * (len(self.u_row) + 1)), file=f)
      print("\npivotes" + str(self.pivots) + "\n", file=f)
