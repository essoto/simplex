from FileManager import FileManager
from Simplex import Simplex


class BigM(Simplex):
    M = 10000

    def __init__(self, problem, filename):
        super().__init__(problem, filename)

    def setup(self):

        if str.upper(self.problem.objective) == "MIN":
            self.u_row = [-x for x in self.u_row]

        for current_row, operation in enumerate(self.operators):

            if operation == '<=':  # +S
                self.add_var(current_row, 1)
                self.u_row.append(0)

            elif operation == '>=':  # +R -S

                r_column = len(self.constrains[current_row])
                self.add_var(current_row, 1)
                self.add_var(current_row, -1)
                self.u_row.append(0)
                self.u_row.append(0)
                self.add_m(current_row, r_column)

            else:  # +R

                r_column = len(self.constrains[current_row])
                self.add_var(current_row, 1)
                self.u_row.append(0)
                self.add_m(current_row, r_column)

        self.setup_u_row()

    def add_m(self, i, j):

        self.results[0] += -self.M * self.results[i + 1]

        for k, value in enumerate(self.constrains[i]):
            if k != j:
                self.u_row[k] += self.M * value


"""bigM = BigM(FileManager.process_file('big_m_example.txt'))

bigM.setup()
bigM.solve()"""
