import math
import sys

from FileManager import FileManager


class Simplex:
    problem = None

    filename = ''

    u_row = []
    results = []
    operators = []
    constrains = []
    pivots = []

    def __init__(self, problem, filename):

        self.problem = problem
        self.filename = filename
        self.u_row = problem.matrix[0][:-1]
        self.results = problem.get_results()
        self.operators = problem.get_restrictions()
        self.constrains = problem.get_matrix_without_u()

    def setup(self):

        if str.upper(self.problem.objective) == "MIN":
            self.u_row = [-x for x in self.u_row]

        for i, operation in enumerate(self.operators):

            if operation == '<=':  # +S
                self.add_var(i, 1)
                self.u_row.append(0)

            else:
                print('ERROR')
                sys.exit(0)

        self.setup_u_row()

    def setup_u_row(self):

        self.u_row = [-x for x in self.u_row]

    def add_var(self, index, var):
        for j, row in enumerate(self.constrains):

            if index == j:
                row.append(var)
            else:
                row.append(0)

    def get_pivot_column(self):
        min_index = 0
        min_item = math.inf

        for index, item in enumerate(self.u_row):

            if item < min_item:
                min_item = item
                min_index = index

        return min_index

    def get_pivot_row(self, c):
        min_index = 0
        min_item = math.inf

        for index, row in enumerate(self.constrains):

            if row[c] > 0:

                if (self.results[index + 1] / row[c]) < min_item:
                    min_item = self.results[index + 1] / row[c]
                    min_index = index

        return min_index

    def reduce(self):

        pivot_column = self.get_pivot_column()
        pivot_row = self.get_pivot_row(pivot_column)
        pivot_value = self.constrains[pivot_row][pivot_column]

        self.pivots.append((pivot_row + 1, pivot_column))

        # reduce pivot row, pivot value equals to 1
        for k, value in enumerate(self.constrains[pivot_row]):
            self.constrains[pivot_row][k] = value / pivot_value

        # apply same operation to result array
        self.results[pivot_row + 1] /= pivot_value

        # reduce the rest of the rows to zero
        for i, row in enumerate(self.constrains):

            if i != pivot_row:
                aux_value = self.constrains[i][pivot_column]

                for j, value in enumerate(self.constrains[i]):
                    self.constrains[i][j] = value - (aux_value * self.constrains[pivot_row][j])

                # apply same operation to result array
                self.results[i + 1] -= (aux_value * self.results[pivot_row + 1])

        aux_value = self.u_row[pivot_column]

        # reduce the u row to zero
        for i, value in enumerate(self.u_row):
            self.u_row[i] = value - (aux_value * self.constrains[pivot_row][i])

        # apply same operation to result array
        self.results[0] -= (aux_value * self.results[pivot_row + 1])

    def is_done(self):
        for value in self.u_row:
            if value < 0:
                return False

        return True

    def solve(self):

        while not self.is_done():
            self.print_table()
            self.reduce()

        self.print_table()


    def print_table(self):

        x = FileManager.generateSolutionFileName(self.filename)

        with open(x, 'a') as f:

            for value in self.u_row:
                print('%7.1f,' % value, end='', file=f)

            print('%7.1f' % self.results[0], file=f)

            for i, row in enumerate(self.constrains):

                for j, value in enumerate(row):
                    print('%7.1f,' % value, end='', file=f)

                print('%7.1f' % self.results[i + 1], file=f)

            print('-' * (8 * (len(self.u_row) + 1)), file=f)

            print("\npivotes" + str(self.pivots) + "\n", file=f)


"""simplex = Simplex(FileManager.process_file('simplex_example.txt'))

simplex.setup()
simplex.solve()"""
